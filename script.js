const sounds = ["🧘‍♂️", "🔔", "😈", "🏫", "😭", "🕐", "💬", "😡", "🏆"];
const buttons = document.getElementById("links");

const stopSounds = () => {
  sounds.forEach((sound) => {
    const currentSound = document.getElementById(sound);
    currentSound.pause();
    currentSound.currentTime = 0;
  });
};

sounds.forEach((sound) => {
  const button = document.createElement("button");
  button.classList.add("button");
  button.innerText = sound;
  button.addEventListener("click", () => {
    stopSounds();
    document.getElementById(sound).play();
  });
  buttons.appendChild(button);
});